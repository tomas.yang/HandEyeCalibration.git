# HandEyeCalibration

#### 项目介绍
手眼系统的标定仿真

#### 软件架构
软件运行环境
* OpenCV
* RobWork
* Windows 10 64bit

#### 软件安装
1. 首先需要配置好软件运行环境，安装OpenCV3.4.x。
2. 安装[RobWork](http://www.robwork.dk/installation/windows.html#introduction)（含RobWorkStudio）。推荐采用源码方式安装，推荐从[Gitee上的RobWork镜像](https://gitee.com/tomas.yang/RobWork)下载源码（master-branch），比直接从官网注册和下载快速很多。直接下载master分支，命令如下：```git clone -b master https://gitee.com/tomas.yang/RobWork```。目前（截至2021-06-29）RobWork源码有较大调整，需要另外编译Eigen，QHull等第三方库。旧版本的RobWork也可以满足本项目要求，因此这里采用的RobWork版本为7b64122775，git的切换命令如下：```git reset --hard 7b64122775``` 。除必须的依赖外，本项目没有编译可选的RobWork依赖，如Xerces，SWIG，Google Test等。
3. 配置好环境变量path，将RobWork、RobWorkStudio、OpenCV编译或安装的bin文件夹路径添加进去。
4. 然后在src目录下新建build文件夹，然后进入build文件夹，在CMD中运行CMake命令（或者通过CMake Gui指定源码位置和编译生成结果文件路径）：'cmake .. -G "Visual Studio 15 2017 Win64"' 。
5. 使用VS2017打开build目录下RobotHandEyeCalibrationSimulator.sln文件，采用Release，X64方式编译。
6. 编译完成后，在src目录下会libs和bin文件夹，其中libs/Release/目录含有RobotHandEyeCalibrationSimulator.lib；bin/Release/目录含有CalibrationCalculator.exe文件。

#### 软件运行
1. 拷贝src目录下的run.bat.template和RobWorkStudio.ini.template至libs文件夹下，重新命名为run.bat和RobWorkStudio.ini
2. 更新RobWorkStudio.ini内容如下：

```
[Plugins]

UserPlugin\DockArea=2
UserPlugin\Filename=RobotHandEyeCalibrationSimulator
UserPlugin\Path=E:/Code/HandEyeCalibration/src/libs/Release
UserPlugin\Visible=true

```

3. 更新run.bat脚本,设置RobWorkStudio.ini路径和仿真场景路径如下：

```
RobWorkStudio.exe --ini-file RobWorkStudio.ini "E:/Code/HandEyeCalibration/workcell_scene/arvp/arvp.handeye.wc.xml"

```

4. 如果在高分屏上运行，显示的界面可能存在控件或文字显示不全，或重叠的情况，有两个解决方案，能够获得同样的效果：1）将src目录下的qt.conf.template文件拷贝至RobWorkStudio.exe文件同一目录下，再运行run.bat文件。2）或者鼠标右键RobWorkStudio.exe文件，点击【属性】-->【兼容性】-->【更改高DPI设置】-->选择最下方【替代高DPI缩放行为】-->下拉框中选择“系统”选项-->点击【应用】保存更改。

#### 标定图像和机器人位姿采集
1. 点击“setup”按钮将机器人移动到采集起始位置
2. 点击“shoot”按钮进行拍摄，同时拍摄后的图片自动存储在run.bat脚本的目录下
3. 使用RobWorkStudio自带的jog工具更改机器人关节角度或末端位置和姿态，移动完成后再点击“shoot”按钮，再次进行拍摄。
4. 重复第3步骤20~30次，获得多组图像。
5. 点击“save”保存以上所有拍摄图像对应的机器人末端位置和姿态，3行4列矩阵（省略了最后1行[0 0 0 1]）,按行依次排列，得到myfile.txt
6. 点击“calibrate”进行标定计算（截至目前尚未集成该步骤）。

#### 标定计算
运行上一步生成的CalibrationCalculator.exe文件，输入图像文件和myfile.txt文件的路径，进行手眼标定计算。

#### 下一步计划
整合标定计算部分至RobWorkStudio Plugin中。